import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";


@Injectable()
export class ConsultantMgmtService {
  constructor(private _networkService: NetworkService) {}

  getConsultantProfileApi(query) {
    return this._networkService.get(
      "api/consultant/profile?" + query,
      null,
      null,
      "bearer"
    );
  }

  
  
  updatecConsultantStatus(user: any) {
    return this._networkService.put("api/user/", user, null, "bearer");
  }
}
