import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ConsultantManagementRoutingModule } from "./consultant-management-routing.module";
import { ConsultantManagementComponent } from "./consultant-management.component";
import { ViewConsultantComponent } from "./view-consultant/view-consultant.component";
import { SharedModule } from "../../shared/shared.module";
import { ConsultantMgmtService } from "./consultant.service";

@NgModule({
  declarations: [ConsultantManagementComponent, ViewConsultantComponent],
  imports: [CommonModule, ConsultantManagementRoutingModule, SharedModule],
  providers: [ConsultantMgmtService]
})
export class ConsultantManagementModule {}
