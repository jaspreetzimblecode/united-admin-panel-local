import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultantManagementComponent } from './consultant-management.component';
import { ViewConsultantComponent } from './view-consultant/view-consultant.component';


const routes: Routes = [
  {
    path: '', component: ConsultantManagementComponent,
    children: [
        { path: '', redirectTo: '/main/consultant-management/view-consultant', pathMatch: 'full' },
        { path: "view-consultant", component: ViewConsultantComponent },

    ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultantManagementRoutingModule { }
