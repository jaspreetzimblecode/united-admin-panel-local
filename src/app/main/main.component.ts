import { Component, OnInit, OnDestroy } from "@angular/core";
import { ChatService } from "./chat.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit, OnDestroy {
  constructor(private chatService: ChatService) {
    this.chatService.createConnetion();
  }

  ngOnInit() {}
  ngOnDestroy() {
    this.chatService.disconnectConnetion();
  }
}
