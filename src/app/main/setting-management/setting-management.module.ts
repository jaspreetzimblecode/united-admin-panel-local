import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingManagementRoutingModule } from './setting-management-routing.module';
import { SettingManagementComponent } from './setting-managemen.component';
import { ViewSettingComponent } from './view-setting/view-setting.component';
import { AccountComponent } from './account/account.component';
import { PasswordComponent } from './password/password.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { BlocklistComponent } from './blocklist/blocklist.component';

import { SettingService } from './setting.service';
import { SharedModule } from '../../shared/shared.module';
import { FileService } from '../../shared/fileService';


@NgModule({
  declarations: [SettingManagementComponent, ViewSettingComponent,AccountComponent,PasswordComponent,NotificationsComponent,BlocklistComponent],
  imports: [
    CommonModule,
    SettingManagementRoutingModule,
    SharedModule
  ],
  providers:[FileService,SettingService]
})
export class SettingManagementModule { }
