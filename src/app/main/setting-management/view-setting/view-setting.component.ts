import { Component, OnInit } from '@angular/core';
import { SettingService } from '../setting.service';
import { AccountComponent } from '../account/account.component';
import { PasswordComponent } from '../password/password.component'
import { NotificationsComponent } from '../notifications/notifications.component'
import { BlocklistComponent } from '../blocklist/blocklist.component'
import { FileService } from '../../../shared/fileService';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-view-setting',
  templateUrl: './view-setting.component.html',
  styleUrls: ['./view-setting.component.css']
})
export class ViewSettingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
