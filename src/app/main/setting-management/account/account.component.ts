

import { Component, OnInit } from "@angular/core";
import { SettingService } from "../setting.service";
import { MatSnackBar } from "@angular/material";
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  isLoading: boolean = false;
  constructor(private settingService: SettingService, private _snackBar: MatSnackBar) {}

  ngOnInit() {}
  selectedFile: File;
  attachmentUrl: any = "";
  onFileChanged(event) {
    this.isLoading = true;
    this.selectedFile = event.target.files[0];
    console.log("selected FIle", this.selectedFile);
    this.settingService.uploadImage(this.selectedFile).subscribe(
      res => {

        this.isLoading = false;
        if (res["message"] == "Success") {
          this.attachmentUrl = res.data.message.url;
          console.log(this.attachmentUrl);
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  postImage() {
    let finalObj;
    finalObj["url"] = this.attachmentUrl;
    finalObj["type"] = "pdf";
    this.isLoading = true;
    this.settingService.postimageApi(finalObj).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this._snackBar.open("Image uploaded successfully", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["success"]
          });
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  queryParams() {
    let query = "";
    
    
    return query;
  }

  // getSettingpdf() {
  //   let queryParam = "";
  //   var queryValue = this.queryParams();
  //   this.isLoading = true;
  //   this.settingService.getsettingApi(queryValue).subscribe(
  //     res => {
  //       this.isLoading = false;
  //       if ((res["message"] = "success")) {
  //         this.setting = res["data"]["bookingList"];

  //         console.log(this.setting);
  //       } else {
  //         this.setting = [];
  //       }
  //       this.isLoading = false;
  //     },
  //     err => {
  //       this.isLoading = false;
  //     }
  //   );
  // }

  getImages: any;
  //   getImage() {
  //      this.isLoading = true;
  //     this.settingService.getimageApi().subscribe(res => {
  //       this.isLoading = false;
  //       if (res["message"] == "Success") {
  //         this.getImages = res.data.url;
  //       } else {
  //       }
  //     }, err => {
  // this.isLoading = false;
  //     });
  //   }
}
