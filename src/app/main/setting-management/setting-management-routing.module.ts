import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SettingManagementComponent } from "./setting-managemen.component";
import { ViewSettingComponent } from "./view-setting/view-setting.component";
import { AccountComponent } from "./account/account.component";
import { PasswordComponent } from "./password/password.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { BlocklistComponent } from "./blocklist/blocklist.component";

const routes: Routes = [
  {
    path: "",
    component: SettingManagementComponent,
    children: [
      { path: "", redirectTo: "/main/setting-management/upload", pathMatch: "full" },
      { path: "terms", component: AccountComponent },
      { path: "upload", component: PasswordComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingManagementRoutingModule {}
