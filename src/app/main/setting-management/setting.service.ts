import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";

@Injectable({
  providedIn: "root"
})
export class SettingService {
  constructor(private networkService: NetworkService) {}

  getDocumentApi() {
    return this.networkService.get("api/webview/document", null, null, "bearer");
  }
  getPrivacyApi() {
    return this.networkService.get("api/webviews/privacyPolicy", null, null, "bearer");
  }
  getConsultantTermsApi() {
    return this.networkService.get("api/webviews/consultantTermsConditions", null, null, "bearer");
  }
  getConsultantPolicyApi() {
    return this.networkService.get("api/webviews/consultantPrivacyPolicy", null, null, "bearer");
  }

  postimageApi(body: any) {
    return this.networkService.post("api/webview", body, null, "bearer");
  }
  uploadImage(image: any) {
    const formData = new FormData();

    formData.append("image", image);
    return this.networkService.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }
}
