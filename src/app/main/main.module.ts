import { NgModule, APP_INITIALIZER } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MainRoutingModule } from "./main-routing.module";
import { MainComponent } from "./main.component";
import { FullComponent } from "../layouts/full/full.component";
import { AppSidebarComponent } from "../layouts/full/sidebar/sidebar.component";
import { AppHeaderComponent } from "../layouts/full/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { UserManagementModule } from "./user-management/user-management.module";
import { BookingManagementModule } from "./booking-management/booking-management.module";
import { ConsultantManagementModule } from "./consultant-management/consultant-management.module";
import { SettingManagementModule } from "./setting-management/setting-management.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ChatService } from "./chat.service";

@NgModule({
  declarations: [MainComponent, FullComponent, AppSidebarComponent, AppHeaderComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    SettingManagementModule,
    UserManagementModule,
    BookingManagementModule,
    ConsultantManagementModule,
    DashboardModule,
    SharedModule

    // DemoMaterialModule
  ],
  providers: [ChatService]
})
export class MainModule {}
// export function initFunction(config: ChatService) {
//   return config.createConnetion();
// }
