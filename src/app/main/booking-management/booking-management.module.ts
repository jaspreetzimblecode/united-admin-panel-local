import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingManagementRoutingModule } from './booking-management-routing.module';
import { BookingManagementComponent } from './booking-management.component';
import { ViewBookingComponent } from './view-booking/view-booking.component';
import { FutureBookingComponent } from './future-booking/future-booking.component';
import { PastBookingComponent } from './past-booking/past-booking.component';
import { BookingService } from './booking.service';
import { SharedModule } from '../../shared/shared.module';
import { FileService } from '../../shared/fileService';


@NgModule({
  declarations: [BookingManagementComponent, ViewBookingComponent,FutureBookingComponent,PastBookingComponent],
  imports: [
    CommonModule,
    BookingManagementRoutingModule,
    SharedModule
  ],
  providers:[FileService,BookingService]
})
export class BookingManagementModule { }
