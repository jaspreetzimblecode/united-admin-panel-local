import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BookingManagementComponent } from "./booking-management.component";
import { ViewBookingComponent } from "./view-booking/view-booking.component";
import { PastBookingComponent } from "./past-booking/past-booking.component";
import { FutureBookingComponent } from "./future-booking/future-booking.component";

const routes: Routes = [
  {
    path: "",
    component: BookingManagementComponent,
    children: [
      { path: "", redirectTo: "/main/booking-management/past-booking", pathMatch: "full" },
      { path: "view-booking", component: ViewBookingComponent },
      { path: "past-booking", component: PastBookingComponent },
      { path: "future-booking", component: FutureBookingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingManagementRoutingModule {}
