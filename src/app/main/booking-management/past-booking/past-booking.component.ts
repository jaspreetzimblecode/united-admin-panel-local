import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
//import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { BookingService } from "../booking.service";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-past-booking",
  templateUrl: "./past-booking.component.html",
  styleUrls: ["./past-booking.component.css"]
})
export class PastBookingComponent implements OnInit {
  userDetails;
  rowDetail: any;
  rowImages: any;
  query: string;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "scheduled", name: "Scheduled" },
    { id: "completed", name: "Completed" },
    { id: "in-progress", name: "In-Progress" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
  // filters end

  bookingList = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean = false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(
    private bookingService: BookingService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getBookingList();
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all")
      query = query + "status=" + this.searchUserStatus + "&";

    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim() + "&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim() + "&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)
  getBookingList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    this.isLoading = true;
    this.bookingService.getPastListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "success")) {
          this.bookingList = res["data"]["bookingList"];

          console.log(this.bookingList);
        } else {
          this.bookingList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      //     /** spinner ends after 5 seconds */
      this.isLoading = false;
    }, 2000);
    (this.searchUserStatus = ""),
      (this.searchText = ""),
      (this.searchBy = ""),
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getBookingList();
  }

  searchFiltersBtn() {
    this.getBookingList();
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      newStatus: ["", [Validators.required]]
    });
  }

  updateUserStatusModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md"
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.rowDetail = row;
    this.statusFormSubmitted = false;
    this.prepareUpdateStatusForm();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  // openImageModal(row, content, btn) {
  //   this.modelOptions = {
  //     backdrop: "static",
  //     keyboard: false,
  //     size:'md'
  //   };
  //   btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

  //   this.rowImages = row;
  //   console.log(this.rowImages)
  //   this.modalRef = this.modalService.open(content, this.modelOptions);
  //   this.modalRef.result.then(
  //     result => {
  //       this.closeResult = `Closed with: ${result}`;
  //       this.rowImages = [];
  //     },
  //     reason => {
  //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //     }
  //   );
  // }

  //  updateUserStatus(test) {
  //    this.statusFormSubmitted = true;
  //    if (this.statusForm.valid) {
  //      this.isLoading = true ;
  //      let finalObj = {
  //        userId: this.rowDetail["userId"],
  //        status: this.statusForm.controls.newStatus.value
  //      };
  //      this.bookingService.updateUserStatus(finalObj).subscribe(
  //        res => {
  //          this.isLoading = false;
  //          if (res["message"] == "Success") {
  //            this._snackBar.open("User status updated successfully!","",{
  //              duration: 5000,
  //              horizontalPosition:'right',
  //              verticalPosition:'top',
  //              panelClass: ['success']
  //            });
  //           // Swal.fire("Success", "Officer updated successfully!", "success");
  //            this.getUserList();
  //            this.modalRef.close();
  //            this.statusForm.reset();

  //          } else {
  //          }
  //        },
  //        err => {
  //          this.isLoading = false;
  //        }
  //      );
  //    }
  //  }

  swalImage(image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400
    });
  }
  //// END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
