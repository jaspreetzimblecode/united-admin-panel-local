import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";

@Injectable({
  providedIn: "root"
})
export class BookingService {
  constructor(private networkService: NetworkService) {}
  getPastListApi(query: any) {
    return this.networkService.get("api/booking?status=completed&" + query, null, null, "bearer");
  }

  getFutureListApi(query: any) {
    return this.networkService.get("api/booking?status=booked&" + query, null, null, "bearer");
  }
}
