import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { BookingService } from '../booking.service';
import { FutureBookingComponent } from '../future-booking/future-booking.component';
import { PastBookingComponent } from '../past-booking/past-booking.component'
import { FileService } from '../../../shared/fileService';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-view-booking',
  templateUrl: './view-booking.component.html',
  styleUrls: ['./view-booking.component.scss']
})
export class ViewBookingComponent implements OnInit {
 isLoading:boolean = false;
  bookingList;
  searchUserStatus;
  searchText;
  searchStartDate;
  searchEndDate;
  searchBy;
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  downloadList: Array<object> = [];
  

  constructor(private fileService: FileService,private bookingservice:BookingService) { }

  ngOnInit() {
     //this.getBookingList();
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    if (this.searchBy && this.searchBy == "userMobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=%2B1" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "stripeId" && this.searchText && this.searchText !== "") {
      query = query + "stripeId=" + this.searchText.trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startTime=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endTime=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }

//   getBookingList() {
//     let queryParam = "";
//     var queryValue = this.queryParams();
//     this.isLoading = true;
// console.log(queryValue);
//     this.bookingservice.getBookingListApi(queryValue).subscribe (
//       res => {
//         this.isLoading = false;
//         if ((res["message"] = "Success")) {
//           this.bookingList = res["data"]["bookingList"];
//           console.log(this.bookingList)
//         } else {
//           this.bookingList = [];
//         }
//         this.isLoading = false;
//       },
//       err => {
//         this.isLoading = false;
//       }
//     );
//   }

  searchFiltersBtn() {
    //this.getBookingList();
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading;
    }, 2000);
    (this.searchText = ""),
      (this.searchUserStatus = ""),
      (this.searchBy = ""),
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    //this.getBookingList();
  }
  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }

  remarks(text) { 
Swal.fire(text)
  }
  exportAsXLSX(excelData:any) {
    var arr2 = []; // create an empty array
excelData.forEach(function(arr) {
    arr2.push({UserName:arr.userFullName,UserMobile:arr.userMobile,CelebrityName:arr.celebrityFullName,CelebrityAmount:arr.celebrityAmount,TotalAmount:arr.amount,CardType:arr.cardScheme,StripeId:arr.stripeId,MaskedCard:arr.maskedCard,Remarks:arr.remarks,Status:arr.status,TransactionStatus:arr.transactionStatus,RefundStatus:arr.refundStatus}) // push in the array
}, this);
    this.fileService.exportAsExcelFile(arr2, "excelFile");
  }

}
