import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
//import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { UserMgmtService } from "../user.service";
import { MatSnackBar } from "@angular/material";
import { ChatService } from "../../chat.service";

@Component({
  selector: "app-view-user",
  templateUrl: "./view-user.component.html",
  styleUrls: ["./view-user.component.scss"]
})
export class ViewUserComponent implements OnInit {
  userDetails;
  rowDetail: any;
  rowImages: any;
  query: string;
  messageText: string;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  messageDetailList: Array<any> = [];
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
  // filters end

  userList = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean = false;
  userDataFrom: string;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(
    private userMgmtService: UserMgmtService,
    private ChatService: ChatService,

    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {
    this.userDataFrom = localStorage.getItem("userd");
    console.log(this.userDataFrom);
  }

  ngOnInit() {
    // this.getUserList();
    console.log("init");
    this.ChatService.getMessages().subscribe(data => {
      console.log("got Message Dest", data);
      this.messageDetailList.push(data);

      console.log("final", this.messageDetailList);
    });
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all")
      query = query + "status=" + this.searchUserStatus + "&";

    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim() + "&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim() + "&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)
  getUserList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    this.isLoading = true;
    this.userMgmtService.getUserListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "success")) {
          this.userList = res["data"];

          console.log(this.userList);
        } else {
          this.userList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      //     /** spinner ends after 5 seconds */
      this.isLoading = false;
    }, 2000);
    (this.searchUserStatus = ""),
      (this.searchText = ""),
      (this.searchBy = ""),
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getUserList();
  }

  searchFiltersBtn() {
    this.getUserList();
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      newStatus: ["", [Validators.required]]
    });
  }

  updateUserStatusModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md"
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.rowDetail = row;
    this.statusFormSubmitted = false;
    this.prepareUpdateStatusForm();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  updateUserStatus(test) {
    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.isLoading = true;
      let finalObj = {
        userId: this.rowDetail["userId"],
        status: this.statusForm.controls.newStatus.value
      };
      this.userMgmtService.updateUserStatus(finalObj).subscribe(
        res => {
          this.isLoading = false;
          if (res["message"] == "Success") {
            this._snackBar.open("User status updated successfully!", "", {
              duration: 5000,
              horizontalPosition: "right",
              verticalPosition: "top",
              panelClass: ["success"]
            });
            // Swal.fire("Success", "Officer updated successfully!", "success");
            this.getUserList();
            this.modalRef.close();
            this.statusForm.reset();
          } else {
          }
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }

  swalImage(image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400
    });
  }

  sendMessage() {
    let message = {
      senderId: this.userDataFrom,
      receiverId: this.searchBy,
      type: "text",
      sentAt: Math.round(new Date().getTime() / 1000),
      message: this.messageText
    };

    this.ChatService.pushData(message);
    // this.messageDetailList.push(message);
    console.log("message", this.messageDetailList);
  }

  //// END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
