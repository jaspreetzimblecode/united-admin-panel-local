import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagementComponent } from './user-management/user-management.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { ViewReportedComponent } from './view-reported/view-reported.component';


const routes: Routes = [
  {path:"",component:UserManagementComponent,
children:[
  { path: '', redirectTo: '/main/user-management/view-user', pathMatch: 'full' },
  {path:"view-user",component:ViewUserComponent},
  // {path:"view-reported",component:ViewReportedComponent}
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserManagementRoutingModule { }
