import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";


@Injectable()
export class UserMgmtService {
  constructor(private _networkService: NetworkService) {}

  getUserListApi(query) {
    return this._networkService.get(
      "api/user/userList?" + query,
      null,
      null,
      "bearer"
    );
  }

  getReportedUserListApi(query) {
    return this._networkService.get(
      "api/reportUsers?" + query,
      null,
      null,
      "bearer"
    );
  }
  
  updateUserStatus(user: any) {
    return this._networkService.put("api/user/", user, null, "bearer");
  }
}
