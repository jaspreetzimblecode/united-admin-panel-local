import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { UserManagementRoutingModule } from "./user-management-routing.module";
import { UserManagementComponent } from "./user-management/user-management.component";
import { ViewUserComponent } from "./view-user/view-user.component";
import { SharedModule } from "../../shared/shared.module";
import { UserMgmtService } from "./user.service";
import { ViewReportedComponent } from "./view-reported/view-reported.component";
import { ChatService } from "../chat.service";

@NgModule({
  declarations: [UserManagementComponent, ViewUserComponent, ViewReportedComponent],
  imports: [CommonModule, UserManagementRoutingModule, SharedModule],
  providers: [UserMgmtService]
})
export class UserManagementModule {}
