import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { CardComponent } from './card/card.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { LineChartsComponent } from './line-charts/line-charts.component';
import { DashboardService } from './dashboard.service';
import { ChartsModule,ThemeService } from 'ng2-charts';
import { FileService } from '../../shared/fileService';
 
@NgModule({
  imports: [
    ChartsModule,
    CommonModule,
    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [DashboardComponent,CardComponent,PieChartComponent,LineChartsComponent],
  providers:[ThemeService,DashboardService]
})
export class DashboardModule {}
