import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { CommonService } from '../../../shared/common.service';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  totalUsers;
  localStore ;
  totalProjects;
  totalEvents;
  totalDispatchers;
  totalFundings;
  totalRevenue;
  totalBooking;

  isLoading:boolean = false;
  constructor(private dashboardService:DashboardService,private commonservice:CommonService) { }

  ngOnInit() {
    this.localStore = this.commonservice.getUser();
       this.getDashboardCount();

  }

  // dashboard cards 
  getDashboardCount() {
    this.isLoading = true;
    this.dashboardService.getDashboardCount().subscribe(res => {
      this.isLoading = false;
      if ((res["message"] = "Success")) {
        this.totalUsers = res["data"]["totalUsers"];
        this.totalProjects= res["data"]["totalProjects"];
        this.totalBooking = res["data"]["totalBooking"];
        this.totalFundings = res["data"]["totalFundings"];
        this.totalRevenue = res["data"]["totalRevenue"];     
      }
    });
  }
// dashboard cards end 
}
