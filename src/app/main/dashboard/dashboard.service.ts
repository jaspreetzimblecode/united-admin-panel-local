import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";

@Injectable()
export class DashboardService {
  constructor(private _networkService: NetworkService) { }

  getDashboardCount() {
    return this._networkService.get("api/dashboard", null, null, "bearer");
  }

  getUserMonthlyApi() {
    return this._networkService.get(
      "api/dashboard/monthlyUserData",
      null,
      null,
      "bearer"
    );
  }

  getUserPieChartDataApi() {
    return this._networkService.get("api/dashboard/userData")
  }

  getProjectPieChartDataApi() {
    return this._networkService.get("api/dashboard/projectData")
  }

  getEventPieChartDataApi() {
    return this._networkService.get("api/dashboard/eventData")
  }

  getProjectLineChartDataApi() {
    return this._networkService.get("api/dashboard/monthlyProjectData")
  }

  getBookingMonthlyApi() {
    return this._networkService.get(
      "api/dashboard/monthlyBooking",
      null,
      null,
      "bearer"
    );
  }
 
  getBookingPieChartDataApi() {
    return this._networkService.get(
      "api/dashboard/BookingData",
      null,
      null,
      "bearer"
    );
  }

  getFundingPieChartDataApi () {
    return this._networkService.get(
      "api/dashboard/fundingData",
      null,
      null,
      "bearer"
    );
  }

}
