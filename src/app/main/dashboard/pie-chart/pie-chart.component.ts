import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts'; 
import { CommonService } from '../../../shared/common.service';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  localStore:any;
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(0,139,139)', 'rgba(189,183,109)', 'rgba(173,255,47)', 'rgba(219, 112, 147)', 'rgb(34, 139, 34)', 'rgba(175,238,238)','rgb(147, 112, 219)','rgba(255, 215, 0)'],
    },
  ]; 
  
  // save data from api
  userPieChartData: {};
  bookingPieChartData:{};
  eventPieChartData: {};
  fundingPieChartData:{};
  projectPieChartData: {};
  // ends

   //save name,value 
   userValueData: Array<number> = [];
   userStatusNameData: Array<string> = [];
   eventValueData: Array<number> = [];
   eventStatusNameData: Array<string> = [];
   projectValueData: Array<number> = [];
   projectStatusNameData: Array<string> = [];
   fundingValueData:Array<number> = [];
   fundingStatusNameData:Array<string> = [];
   //end

  constructor(private dashboardService:DashboardService,private commonservice:CommonService) { }

  ngOnInit() {
    this.localStore = this.commonservice.getUser();
    if(this.localStore['role'] =='admin') {
       //this.getProjectPieChartData();
       //this.getEventPieChartData();
       this.getUserPieChartData();
       //this.getFundingPieChartData();
       this.getBookingPieChartData();
    }
   
  }

  getProjectPieChartData() {
    this.dashboardService.getProjectPieChartDataApi().subscribe(res => {
      if (!res["msg"]) {
        this.projectPieChartData = res;
        for (let i = 0; i < res["data"].length; i++) {
          this.projectValueData.push(res["data"][i].value);
          this.projectStatusNameData.push(res["data"][i].name)
        }
      }
    })
  }

  getEventPieChartData() {
    this.dashboardService.getEventPieChartDataApi().subscribe(res => {
      if (!res["msg"]) {
        this.eventPieChartData = res;
        for (let i = 0; i < res["data"].length; i++) {
          this.eventValueData.push(res["data"][i].value);
          this.eventStatusNameData.push(res["data"][i].name)
        }
      }
    })
  }

  getFundingPieChartData() {
    this.dashboardService.getFundingPieChartDataApi().subscribe(res => {
      if (!res["msg"]) {
        this.fundingPieChartData = res;
        for (let i = 0; i < res["data"].length; i++) {
          this.fundingValueData.push(res["data"][i].value);
          this.fundingStatusNameData.push(res["data"][i].name)
        }
      }
    })
  }

  getUserPieChartData() {
    this.dashboardService.getUserPieChartDataApi().subscribe(res => {
      if (!res["msg"]) {
        this.userPieChartData = res;
        for (let i = 0; i < res["data"].length; i++) {
          this.userValueData.push(res["data"][i].value);
          this.userStatusNameData.push(res["data"][i].name)
        }
      }
    })
  }

  getBookingPieChartData() {
    this.dashboardService.getBookingPieChartDataApi().subscribe(res => {
      if (!res["msg"]) {
        this.bookingPieChartData = res;
        for (let i = 0; i < res["data"].length; i++) {
          this.userValueData.push(res["data"][i].value);
          this.userStatusNameData.push(res["data"][i].name)
        }
      }
    })
  }

}

