import { Injectable } from "@angular/core";
import { Observer } from "rxjs/Observer";
import { Observable } from "rxjs/Observable";
import * as Rx from "rxjs";
import * as io from "socket.io-client";
import { CommonService } from "../shared/common.service";
import { environment } from "../../environments/environment";
import { MainModule } from "./main.module";

export class ChatService {
  observable: Observable<any>;
  socket;
  userDetail: any;
  userId: any;

  constructor(public commonService: CommonService) {
    this.userDetail = this.commonService.getUser();
    if (this.userDetail) {
      console.log("userIdata", this.userId);
      this.socket = io(environment.backendApiURL + "?userId=" + this.userDetail.userId);
      this.createConnetion();
      console.log("connecttion created");
    }
  }
  createConnetion() {
    this.socket = io(environment.backendApiURL + "?userId=" + this.userDetail.userId);
  }
  disconnectConnetion() {
    console.log("DISCONNECT");
    this.socket.emit("disconnect");
  }
  // This one is for send data from angular to node
  pushData(e) {
    this.socket.emit("sendMessage", e);
  }
  userIsTyping(e) {
    console.log("userTyping");
    this.socket.emit("isTyping", e);
  }
  userTypingStopped(e) {
    console.log("typingStopped");
    this.socket.emit("isTypingStop", e);
  }

  getMessages() {
    console.log("getMessages function in chat ");
    let observable = new Observable(observer => {
      this.socket.on("sendMessage", data => {
        console.log("inside get Message", data);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  isTyping() {
    let observable = new Observable(observer => {
      this.socket.on("isTyping", data => {
        console.log("typomg");
        observer.next(data);
      });
    });
    return observable;
  }
  isTypingStop() {
    let observable = new Observable(observer => {
      this.socket.on("isTypingStop", data => {
        console.log("isTypingStop");
        observer.next(data);
      });
    });
    return observable;
  }
}
