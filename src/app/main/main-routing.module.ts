import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { FullComponent } from '../layouts/full/full.component';
// import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthGuardMain } from '../auth/auth-guard.service';
//import { FutureBookingComponent } from './booking-management/future-booking/future-booking.component';


const routes: Routes = [
  // { path: "", redirectTo: "/main/dashboard", pathMatch: "full" },
  {
    path: "main",
    component: MainComponent,
    canActivate: [AuthGuardMain],
    children: [
      { path: "", redirectTo: "/main/user-management", pathMatch: "full" },
    
      { path: 'dashboard', loadChildren: () => import('../main/dashboard/dashboard.module').then(m => m.DashboardModule)},
      { path: 'user-management', loadChildren: () => import('../main/user-management/user-management.module').then(m => m.UserManagementModule)},
      { path: 'report-management', loadChildren: () => import('../main/report-management/report-management.module').then(m => m.ReportManagementModule)},
       { path: 'booking-management', loadChildren: () => import('../main/booking-management/booking-management.module').then(m => m.BookingManagementModule)},
       { path: 'consultant-management', loadChildren: () => import('../main/consultant-management/consultant-management.module').then(m => m.ConsultantManagementModule)},
      { path: 'setting-management', loadChildren: () => import('../main/setting-management/setting-management.module').then(m => m.SettingManagementModule)},
       //{
        // path: "setting-management",
         //component: SettingManagementComponent,
       //},
       //{ path: 'setting', loadChildren: () => import('../main/setting-management/setting-management.module').then(m => m.SettingManagementModule)},
       //{
        // path: "future-booking",
         //component: FutureBookingComponent,
       //},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
