import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportManagementComponent } from './report-management.component';
import { ViewReportComponent } from './view-report/view-report.component';
import { EventReportComponent } from './event-report/event-report.component';
import { ProjectReportComponent } from './project-report/project-report.component';
import { UserReportComponent } from './user-report/user-report.component';

const routes: Routes = [
  {path:"",component:ReportManagementComponent,
children:[
  { path: '', redirectTo: '/main/report-management/user-report', pathMatch: 'full' },
  // {path:"event-report",component:EventReportComponent},
  // {path:"project-report",component:ProjectReportComponent},
  {path:"user-report",component:UserReportComponent},
  // {path:"view-report",component:ViewReportComponent},
]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportManagementRoutingModule { }
