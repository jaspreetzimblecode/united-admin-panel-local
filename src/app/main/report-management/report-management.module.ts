import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportManagementRoutingModule } from './report-management-routing.module';
import { ViewReportComponent } from './view-report/view-report.component';
import { ReportManagementComponent } from './report-management.component';
import { UserReportComponent } from './user-report/user-report.component';
import { EventReportComponent } from './event-report/event-report.component';
import { ProjectReportComponent } from './project-report/project-report.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [ViewReportComponent, ReportManagementComponent, UserReportComponent, EventReportComponent, ProjectReportComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportManagementRoutingModule
  ]
})
export class ReportManagementModule { }
