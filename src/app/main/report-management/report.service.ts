import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private _networkService: NetworkService) {}

  getUserReportApi(query) {
    return this._networkService.get(
      "api/admins/reported/users?" + query,
      null,
      null,
      "bearer"
    );
  }
  getProjectReportApi(query) {
    return this._networkService.get(
      "api/projects/report?" + query,
      null,
      null,
      "bearer"
    );
  }
  getEventReportApi(query) {
    return this._networkService.get(
      "api/events/report?" + query,
      null,
      null,
      "bearer"
    );
  }


}
