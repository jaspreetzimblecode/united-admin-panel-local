import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModalOptions, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from '../report.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-event-report',
  templateUrl: './event-report.component.html',
  styleUrls: ['./event-report.component.css']
})
export class EventReportComponent implements OnInit {
  rowDetail:any;
  reportedEventList:any;
  query: string;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];
    // filters
    searchStartDate: string;
    searchEndDate: string;
    searchUserStatus: string = "all";
    searchBy: string = "all";
    searchText: string;
  // filters end
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean=false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(private reportService: ReportService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    // this.getEventReportList();
  }


  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    
    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
// GET USERS (By Default ALL)
getEventReportList() {
  let queryParam = "";
  var queryValue = this.queryParams();
  this.isLoading = true;
  this.reportService.getEventReportApi(queryValue).subscribe(
    res => {
      this.isLoading = false;
      if ((res["message"] = "Success")) {
        this.reportedEventList = res["data"]["eventsList"];
        console.log(this.reportedEventList,"event")
      } else {
        this.reportedEventList = [];
      }
      this.isLoading = false;
    },
    err => {
      this.isLoading = false;
    }
  );
}

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading= false;
    }, 2000);
    this.searchUserStatus = "",
    this.searchText = "",
    this.searchBy ="",
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    // this.getUserList();
  }

  searchFiltersBtn() {
    // this.getUserList();
  }

  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }

}
