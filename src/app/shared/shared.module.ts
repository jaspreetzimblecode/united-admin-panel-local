import { MenuItems } from "./menu-items/menu-items";
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from "./accordion";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatDatepickerModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatRadioModule,
  MatTableModule,
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatMenuModule
} from "@angular/material";
import { MatTabsModule } from "@angular/material/tabs";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { MatNativeDateModule } from "@angular/material/core";
import { SpinnerComponent } from "./spinner/spinner.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { ChatService } from "../main/chat.service";
@NgModule({
  declarations: [AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective, SpinnerComponent],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatInputModule,
    MatOptionModule,
    MatIconModule,
    MatSelectModule,
    MatTabsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatRadioModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,
    MatButtonModule,
    MatListModule,
    NgxDatatableModule,
    SpinnerComponent
  ],
  providers: [MenuItems]
})
export class SharedModule {}
