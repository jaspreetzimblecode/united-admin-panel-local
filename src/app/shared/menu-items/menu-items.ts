import { Injectable } from "@angular/core";

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  // { state:'dashboard', name: 'Dashboard', type: 'link', icon: 'av_timer' },
  { state: "dashboard", name: "Dashboard", type: "link", icon: "av_timer" },

  { state: "user-management", type: "link", name: "Users", icon: "people_alt" },
  //{ state: 'funding-management', type: 'link', name: 'Fundings', icon: 'people_alt' },
  //{ state: 'event-management', type: 'link', name: 'Events', icon: 'people_alt' },
  //{ state: 'project-management', type: 'link', name: 'Projects', icon: 'people_alt' },
  //{ state: 'report-management', type: 'link', name: 'User report', icon: 'report' },
  // { state: 'celebrity-management/view-celebrity', type: 'link', name: 'Vips', icon: 'verified_user' },
  // { state: 'category-management', type: 'link', name: 'Category', icon: 'dns' },
  { state: "booking-management", type: "link", name: "Bookings", icon: "event_available" },
  //  { state: 'consultant-management', type: 'link', name: 'Consultant', icon: 'report' },
  { state: "setting-management", type: "link", name: "Settings", icon: "settings" }
  // { state: 'snackbar', type: 'link', name: 'Snackbar', icon: 'adb' },
  // { state: 'slider', type: 'link', name: 'Slider', icon: 'developer_mode' },
  // {
  //   state: 'slide-toggle',
  //   type: 'link',
  //   name: 'Slide Toggle',
  //   icon: 'all_inclusive'
  // }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
