import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CommonService } from "../../shared/common.service";
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";
import { HttpResponse } from "@angular/common/http";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  formSubmitted: boolean = false;
  errorMsg: string = "Ok";
  headers = [];
  adminUser: boolean = true;
  clubUser: boolean = false;
  LoginFlag: boolean = true;
  ResetFlag: boolean = false;
  Loginmessage: string = "";
  isLoading: boolean = false;
  constructor(
    private fb: FormBuilder,
    private authServices: AuthService,
    private commonService: CommonService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initialiseForms();
  }
  initialiseForms() {
    this.loginForm = this.fb.group({
      userName: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });
  }

  loginFN() {
    this.router.navigate(["main/dashboard"]);
  }

  login() {
    this.formSubmitted = true;
    if (this.loginForm.valid) {
      let req = {
        email: this.loginForm.controls["userName"].value,
        password: this.loginForm.controls["password"].value,
        isPodcast: "true"
      };
      this.isLoading = true;
      this.authServices.validateLogin(req).subscribe(
        (res: HttpResponse<any>) => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            localStorage.setItem("userd", res.body.data.userId);
            console.log(res);
            this.commonService.setStorage("token", res.headers.get("Authorization"));

            console.log("LOFIN");
            this.router.navigate(["main/user-management/view-user"]);
          } else {
          }
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }
}
