
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from './auth/auth.module';
import { MainModule } from './main/main.module';
import { NetworkService } from './shared/network.service';
import { CommonService } from './shared/common.service';
import { AuthGuardMain } from './auth/auth-guard.service';
import { AppRoutingModule } from './app.routing';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";






const routes: Routes = [
  { path: "", redirectTo: "auth/login", pathMatch: "full" }
];

@NgModule({
  declarations: [
    AppComponent,

   
  ],
  imports: [
    AuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    MainModule,
    NgbModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
  ],
  providers: [NetworkService,CommonService,AuthGuardMain],
  
  bootstrap: [AppComponent]
})
export class AppModule {}
